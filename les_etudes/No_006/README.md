## Implement a FIFO container using two arrays.
Using two arrays Arr and Temp.
When all elements of Arr are populated, copy all elements except that at index i=0 to Temp[0] through Temp[N-2]. Write the current value to temp[N-1]. Finally, copy all elements of Temp to Arr.

### Instructions  

Compile then run:  
```
~$ gcc fifo.c -o fifo -g -Wall -O3 -std=gnu11
~$ ./prob
```

#### Suggested Improvements  

1. Yes, a function implementation would be nice
