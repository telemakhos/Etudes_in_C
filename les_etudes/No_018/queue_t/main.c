#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "queue.h"


void print_queue_int(queue_t *s);

int main(void)
{
    queue_t q;
    queue_new(&q, sizeof(int), NULL);

    int value = 10;
    queue_enqueue(&q, &value);

    value = 11;
    queue_enqueue(&q, &value);

    value = 12;
    queue_enqueue(&q, &value);

    value = 13;
    queue_enqueue(&q, &value);

    value = 14;
    queue_enqueue(&q, &value);

    queue_dequeue(&q);

    print_queue_int(&q);

    queue_destroy(&q);

}


void print_queue_int(queue_t *queue) {
    size_t sz = list_size(queue->list);
    printf("sz: %lu\n", sz);
    int val; // data type of queue elements
    for(unsigned long int i=0; i<sz; i++) {
        list_get_at(queue->list, i, &val);
        printf("%lu : %d\n", i, val);
    }
    printf("\n"); 
}
