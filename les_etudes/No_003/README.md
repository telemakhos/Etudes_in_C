# Linked list of Players possessing a linked list inventory

Create basic player management capability for a classic RPG. This simple program performs, from main(), the addition and removal of characters, each possessing an inventory of items.  


To compile with GCC:  
`~$ gcc lloll.c -o lloll -g -Wall -O3 -std=gnu11`   
Then run the executable file with:  
`~$ ./lloll`  


#### Improvements to make  
1. Give each unique item type in inventory a quantity value. For example, 4 arrows, 1 flask, 2 shark teeth.  
2. Initialize players and player inventory from a file on disk. After play, save the modified file to disk.  
3. Place function declarations and structs in header files.  
