
#include <stdio.h>
#include <stdlib.h>

int comp (const void * elem1, const void * elem2) 
{
    int f = *((int*)elem1);
    int s = *((int*)elem2);
    if (f > s) return  1;
    if (f < s) return -1;
    return 0;
}

int main(int argc, char* argv[]) 
{
    int x[] = {4,5,2,3,1,0,9,8,6,7};

    qsort (x, sizeof(x)/sizeof(*x), sizeof(*x), comp);

    for (int i = 0 ; i < 10 ; i++)
        printf ("%d ", x[i]);

    return 0;
}


/*
Notes:
  In answer to the question: "Is there any library function available in
  C standard library to do sort?"

  The answer was given: 
  "qsort() is the function you're looking for. You call it with a pointer to
  your array of data, the number of elements in that array, the size of each
  element and a comparison function.
  It does its magic and your array is sorted in-place. ..."

  Notice that the comparator function does not use subtraction to determine
  which number is greater/smaller, as this could result in an overflow.
  comp() could also return the answer more concisely like this:
  return (f > s) - (f < s);

Source:
  https://stackoverflow.com/questions/1787996/c-library-function-to-do-sort
*/
