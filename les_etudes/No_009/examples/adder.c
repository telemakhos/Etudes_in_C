#include <stdio.h>
#include <stdlib.h>

struct Result{
    int s; // the sum
    int c; // the carry out
};

void fullAdder(struct Result *res, int cin, int n1, int n2);

int main()
{
    const char * n1 = "11111111";
    const char * n2 = "00000011";
    int cin = 0;
    struct Result result;
    int answer[sizeof(n1)];
    for (int i=sizeof(n1)-1; i>=0; i--) {
        int ain = (n1[i]-'0');
        int bin = (n2[i]-'0');
        fullAdder(&result, cin, ain, bin);
        answer[i] = result.s;
        cin  = result.c;
    }
    printf(".....sum : ");
    for (int i=0; i<(sizeof(answer)/sizeof(answer[1])); i++) {
        printf("%d", answer[i]);
    }
    printf("\noverflow :        %d\n", result.c);
}


void fullAdder(struct Result *res, int cin, int n1, int n2) {
    res->s = (n1 ^ n2) ^ cin;
    res->c = (cin & (n1 ^ n2))  | (n1 & n2);
}
