#include <stdio.h>

void main (void)
{
    char input[128], name[128];
    int favNumber;

    printf ("what is your name?\n");
    fgets (input, 128, stdin);
    sscanf (input, "%s", name);

    printf ("%s, What is your favorite number?\n", name);
    while (1) {
        fgets (input, 128, stdin);
        if (sscanf (input, "%d", &favNumber) == 1) break;
        printf ("I was expecting an integer, please try again!\n");
    }
    printf ("%d is a good number %s.\n", favNumber, name);
}
