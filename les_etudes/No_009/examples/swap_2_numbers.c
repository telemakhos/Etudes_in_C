/*
  How to swap two numbers without using a temporary variable
*/
#include <stdio.h>

int main()
{
    int x = 10, y = 5;
    int method = 3;

    switch(method) {

        case 1 :  // METHOD 1 - Using arithmetic operators
            
            // Code to swap 'x' and 'y'
            x = x + y;  // x now becomes 15
            y = x - y;  // y becomes 10
            x = x - y;  // x becomes 5

            printf("After Swapping: x = %d, y = %d\n", x, y);
            break;

        case 2 :  // METHOD 2 - Using multiplication and division

            // Code to swap 'x' and 'y'
            x = x * y;  // x now becomes 50
            y = x / y;  // y becomes 10
            x = x / y;  // x becomes 5

            printf("After Swapping: x = %d, y = %d\n", x, y);
            break;

        case 3:  // METHOD 3 - Using Bitwise XOR

            // Code to swap 'x' (1010) and 'y' (0101)
            x = x ^ y;  // x now becomes 15 (1111)
            y = x ^ y;  // y becomes 10 (1010)
            x = x ^ y;  // x becomes 5 (0101)

            printf("After Swapping: x = %d, y = %d\n", x, y);
    }
}



/*
Notes:
  1. The multiplication and division approaches will fail if one of the numbers is 0.
  2. Both Arithmetic solutions may cause arithmetic overflow.
  3. When pointers are used to make a swap function, all of the above methods fail when
     both pointers point to the same variable. This danger can be avoided by putting a
     condition before the swapping, as in the following example:

     #include <stdio.h>
     void swap(int *xp, int *yp)
     {
         if (xp == yp) // Check if the two addresses are same
           return;
         *xp = *xp + *yp;
         *yp = *xp - *yp;
         *xp = *xp - *yp;
     }

     int main()
     {
       int x = 10;
       swap(&x, &x);
       printf("After swap(&x, &x): x = %d", x);
       return 0;
     }



Sources:
  https://www.geeksforgeeks.org/swap-two-numbers-without-using-temporary-variable/
*/
