/*
    Use ANSI escape codes to have the terminal emulator several colors.
    Note: we could change the background color too if we desired.
    Additional information here: https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
*/

#include <stdio.h>

#define COLOR_RED     "\x1b[31m"
#define COLOR_GREEN   "\x1b[32m"
#define COLOR_YELLOW  "\x1b[33m"
#define COLOR_BLUE    "\x1b[34m"
#define COLOR_MAGENTA "\x1b[35m"
#define COLOR_CYAN    "\x1b[36m"
#define COLOR_RESET   "\x1b[0m"

int main (int argc, char const *argv[]) {

    printf(COLOR_RED     "This text is RED!"     COLOR_RESET "\n");
    printf(COLOR_GREEN   "This text is GREEN!"   COLOR_RESET "\n");
    printf(COLOR_YELLOW  "This text is YELLOW!"  COLOR_RESET "\n");
    printf(COLOR_BLUE    "This text is BLUE!"    COLOR_RESET "\n");
    printf(COLOR_MAGENTA "This text is MAGENTA!" COLOR_RESET "\n");
    printf(COLOR_CYAN    "This text is CYAN!"    COLOR_RESET "\n");

    return 0;
}
