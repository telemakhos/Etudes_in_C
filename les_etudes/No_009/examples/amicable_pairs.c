#include <stdio.h>
#include <stdlib.h> // atoi()

unsigned long long get_sum_of_proper_divisors(unsigned long long);

int main(int argc, char *argv[])
{
    if(argc==1) 
        printf("\nEnter an integer argument\n");

    // find the proper divisors of "number"
    unsigned long long max_number = (unsigned long long)atoi(argv[1]);
    unsigned long long numbersArr[max_number-1];
    unsigned long long sumsArr[max_number-1];
    unsigned long long count = 0;
    for (unsigned long long number=2; number<max_number; number++) {
        unsigned long long sum;
        sum = get_sum_of_proper_divisors(number);
        printf("The sum of the proper divisors of %llu is %llu\n", number, sum);
        numbersArr[count] = number;
        sumsArr[count] = sum; 
        count += 1;
    }

    // search the numbers and sums arrays for matches (Amicable Numbers)
    for (unsigned long long i=0; i<(max_number-2); i++) {
        //printf("%llu  -  %llu\n", numbersArr[i], sumsArr[i]);
        for (unsigned long long j=0; j<(max_number-2); j++) {
            if ( (numbersArr[i] == sumsArr[j]) &&
                 (numbersArr[j] == sumsArr[i]) &&
                 (i!=j) ) {
                pairs_sum += numbersArr[i];
                printf("Amicable pair: %llu, %llu\n", numbersArr[i], numbersArr[j]);
            }
        }
    }
}


unsigned long long get_sum_of_proper_divisors(unsigned long long number) {
    unsigned long long testnb, evenchk;
    unsigned long long sum = 0;
    for (testnb=1; testnb<(number/2)+1; testnb++) {
        evenchk = number%testnb;
        if (evenchk==0) { // then testnb is a proper divisor of number
            sum += testnb;
            //printf("%llu: %llu %llu\n", testnb, evenchk, remainder);
        }
    }
    return sum;
}
