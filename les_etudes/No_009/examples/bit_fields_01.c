
//// A simple representation of a date 
//struct date
//{
//   unsigned int d;
//   unsigned int m;
//   unsigned int y;
//};

// A space optimized representation of date
struct date
{
   // d, using 5 bits, has a value between 1 and 31
   unsigned int d: 5;
   // m, using 4 bits, has a value between 1 and 12
   unsigned int m: 4;
   unsigned int y;
};


int main()
{
   printf("Size of date is %d bytes\n", sizeof(struct date));
   struct date dt = {31, 12, 2014};
   printf("Date is %d/%d/%d\n", dt.d, dt.m, dt.y);
}

// The above simple representation of ‘date’ takes 12 bytes on a compiler where an unsigned
// int takes 4 bytes.
// The output when using the simple date struct is:
//    Size of date is 12 bytes
//    Date is 31/12/2014

//Since we know that the value of d is always from 1 to 31, value of m is
// from 1 to 12, we can optimize the space using bit fields.
// The output when using the optimized data struct using bit fields is:
//    Size of date is 8 bytes
//    Date is 31/12/2014


/*
Notes:
  
Source:
  https://www.geeksforgeeks.org/bit-fields-c
*/
