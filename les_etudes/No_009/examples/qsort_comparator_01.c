#include <stdio.h>
#include <stdlib.h> // qsort

int values[] = { 88, 56, 100, 2, 25 };

int cmpfunc (const void * a, const void * b) {
   return ( *(int*)a - *(int*)b );
}

int main () {
    int n;

    printf("Before sorting the list is: \n");
    for( n = 0 ; n < 5; n++ ) {
        printf("%d ", values[n]);
    }

    qsort(values, 5, sizeof(int), cmpfunc);

    printf("\nAfter sorting the list is: \n");
    for( n = 0 ; n < 5; n++ ) {   
        printf("%d ", values[n]);
    }
    printf("\n");
}



/*
Notes:
  The C library function void qsort(void *base,
                                    size_t nitems,
                                    size_t size,
                                    int (*compar)(const void *, const void*))
  sorts an array.

  Following is the declaration for qsort() function:
  void qsort(void *base, size_t nitems, size_t size, int (*compar)(const void *, const void*))

Parameters

    base − This is the pointer to the first element of the array to be sorted.

    nitems − This is the number of elements in the array pointed by base.

    size − This is the size in bytes of each element in the array.

    compar − This is the function that compares two elements.

Return Value

This function does not return any value.

************************************************************************
Another way to write the comparitor function is thusly:

int cmpfunc (const void * a, const void * b) //what is it returning?
{
   // qsort() passes in `void*` types because it can't know the actual types being sorted
   // convert those pointers to pointers to int and deref them to get the actual int values

   int val1 = *(int*)a;
   int val2 = *(int*)b;

   // qsort() expects the comparison function to return:
   // 
   //    a negative result if val1 < val2
   //    0 if val1 == val2
   //    a positive result if val1 > val2

   return ( val1 - val2 ); 
}

*************************************************************************

Sources:
  https://www.tutorialspoint.com/c_standard_library/c_function_qsort.htm
  https://stackoverflow.com/questions/27284185/how-does-the-compare-function-in-qsort-work
*/
