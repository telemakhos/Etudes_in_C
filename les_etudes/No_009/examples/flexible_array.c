#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define OFFSETOF(TYPE, ELEMENT) ((size_t)&(((TYPE *)0)->ELEMENT))

struct Cimage {
    char dtype[8];
    unsigned int rows;
    unsigned int cols;
    unsigned int s_array[];
};


int main()
{
    unsigned int rows = 5;
    unsigned int cols = 8;
    struct Cimage *ptr = malloc( OFFSETOF(struct Cimage, s_array) +
                                 (rows*cols*sizeof(unsigned int)) );

    // initialize the array members
    strncpy(ptr->dtype, "uint16", 8);
    ptr->rows = rows;
    ptr->cols = cols;
    for (unsigned int i=0; i<(ptr->rows*ptr->cols); i+=1) {
        ptr->s_array[i] = i * 300;
    }

    // print the elements of the struct
    printf("dtype: %s\n", ptr->dtype);
    for (unsigned int i=0; i<(ptr->rows * ptr->cols); i+=1) {
        printf("%u ", ptr->s_array[i]);
    }
    printf("\n");
}
