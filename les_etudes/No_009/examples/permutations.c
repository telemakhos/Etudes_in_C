#include <stdio.h>
#include <string.h>

void swap(char *x, char *y);
void permute(char *a, int l, int r, int *cptr);

int main()
{
    // define and initialize an array of ints
    int count = 0;
    char str[] = "0123";
    int n = strlen(str);
    permute(str, 0, n-1, &count);

}


/* Function to swap values at two pointers */
void swap(char *x, char *y)
{
    char temp;
    temp = *x;
    *x = *y;
    *y = temp;
}


/* Function to print the permutations of a string
   This function takes four parameters:
   1. String
   2. Starting index of the string
   3. Ending index of the string.
   4. Counter (to identify the Nth permutation) */
void permute(char *a, int l, int r, int *cptr)
{
    int i;
    if (l == r) {
        *cptr += 1;
        printf("%7d  %s\n", *cptr, a);
    }
    else
    {
        for (i = l; i <= r; i++)
        {
            swap((a+l), (a+i));
            permute(a, l+1, r, cptr);
            swap((a+l), (a+i)); //backtrack
        }
    }
}
