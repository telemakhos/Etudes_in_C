#include <stdio.h>
#include <stdlib.h>

/* The calling function takes a single callback as a parameter. */
void PrintTwoNumbers(int (*numberSource)(void)) {
    int val1 = numberSource();
    int val2 = numberSource();
    printf("%d and %d\n", val1, val2);
}

/* A possible callback */
int overNineThousand(void) {
    return (rand()%1000) + 9001;
}

/* Another possible callback. */
int meaningOfLife(void) {
    return 42;
}

/* Here we call PrintTwoNumbers() with three different callbacks. */
int main(void) {
    PrintTwoNumbers(&rand);
    PrintTwoNumbers(&overNineThousand);
    PrintTwoNumbers(&meaningOfLife);
    return 0;
}



/*
  Notes:
  Callbacks have a wide variety of uses, for example in error signaling: a Unix program might
  not want to terminate immediately when it receives SIGTERM, so to make sure that its
  termination is handled properly, it would register the cleanup function as a callback.
  Callbacks may also be used to control whether a function acts or not: Xlib allows custom
  predicates to be specified to determine whether a program wishes to handle an event.

  Note how this (the usage above) is different from simply passing the output of the
  callback function to the calling function, PrintTwoNumbers() - rather than printing
  the same value twice, the PrintTwoNumbers calls the callback as many times as it
  requires. This is one of the two main advantages of callbacks.

  Source:
  https://en.wikipedia.org/wiki/Callback_(computer_programming)
*/
