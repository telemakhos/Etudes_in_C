## Assorted things a programmer should know ... plus some
There is no common thread binding these programs together into this common directory, except that I thought that a C programmer should be aware of them. The number theory and numerical calculation programs are just for fun though.  

### Index  

##### [001 : pointers 1](./examples/pointers_01.c) - To change a local pointer of one function inside another function, one must pass a pointer to the pointer.  

##### [002 : swap 2 numbers](./examples/swap_2_numbers.c) - Swapping the values between two number variables without using a temporary variable (3 methods).  

##### [003 : comparing without subtraction](./examples/comparing_without_subtraction.c) - Since subtraction can result in overflow, perform logical comparisons.

##### [004 : russian peasant multiplication](./examples/russian_peasant.c) - Multiplying two numbers without a multiplication operator.  

##### [005 : primes between two numbers](./examples/primes_between_two_numbers.c) - Find all prime numbers between two integers.

##### [006 : print aligned](./examples/print_aligned.c) - Aligned printing using printf formatting.

##### [007 : pointer to function](./examples/ptr_to_func.c) - How to use a pointer to a function.

##### [008 : qsort comparator 1](./examples/qsort_comparator_01.c) -A simple comparator func for the stdlib qsort().

##### [009 : qsort comparator 2](./examples/qsort_comparator_02.c) - A more complicated comparator func example for qsort().

##### [010 : bit fields 1](./examples/bit_fields_01.c) - Replacing a struct's unsigned ints with appropriate bit fields to save bytes.

##### [011 : bit fields 2](./examples/bit_fields_02.c) - Use of an unnamed struct member to force alignment on the next byte boundary.

##### [012 : callbacks 1](./examples/callbacks_01.c) - A calling function calls the callback as many times as required.

##### [013 : callbacks 2](./examples/callbacks_02.c) - A callback usage illustrating "information hiding", meaning that parameters passed to the function are not publicly exposed.

##### [014 : iterative binary search](./examples/binary_search_iterative.c) - An iterative implementation of binary search.

##### [015: recursive binary search](./examples/binary_search_recursive.c) - An recursive implementation of binary search.
    
##### [016 : xor linked list](./examples/xor_linked_list.c) - An XOR linked list stores the bitwise XOR of the address for "previous" and the address for "next" in one field, instead of in two separate fields like in the usual doubly linked list.

##### [017 : full adder](./examples/adder.c) - Using a full adder, implemented in bitwise operators, to add two eight bit numbers and set an overflow bit.

##### [018 : digits of pow() for large exponents](./examples/digits_large_exponents.c) - Calculate and print every digit in pow() operation, such as a^n. 2^1000 has 302 digits.

##### [019 : digits of large factorial](./examples/digits_large_factorial.c) - Calculate and print the factorial of a number such as 100, the digits of which cannot be represented any and data type.

##### [020 : amicable pairs](./examples/amicable_pairs.c) - Calculate and print all amicable pairs below an integer.

##### [021 : permutations](./examples/permutations.c) - Print all permutations of chars in a string.

##### [022 : drop through](./examples/drop_thru.c) - Illustrating the "fall-through" (or "drop-through") feature of the switch statement

##### [023 : loop unrolling](./examples/loop_unrolling.c) - Illustrating a loop optimization technique called loop-unrolling.

##### [024 : array of pointers to functions](./examples/array_of_fnc_ptrs.c) - Using an array of pointers to functions.  

##### [025  flexible array](./examples/flexible_array.c) - Dynamically allocate memory for an uninitialized array struct member. This used to be called the "struct hack".  

##### [026 : print in colors](./examples/print_in_colors.c) - Use ANSI escape codes to have the terminal emulator print in several colors.  

##### [027 : user input idiom](./examples/user_input.c) - Use fgets to read user input into a buffer string, then use sscanf to read values from that string.

##### [028 : write user input to output file](./examples/output_file.c) - Use fgets to read user input into a buffer, then write to a file.

##### [029 : progress bar](./examples/progress_bar.c) - A progress bar to watch while your code executes.
