#ifndef PLAYER_H_
#define PLAYER_H_

#include "item.h"
#include "list.h"

#define MIN_STAT_VAL 8
#define MAX_STAT_VAL 20
//#define DEFAULT_LIST {list_t list_new(&inventory, sizeof(double), NULL)}
//#define DEFAULT_PLAYER {"Starchild", 19, 0, "human", "ranger", 12, 12, 12, 12, 12, 12, 50, 0, 0, DEFAULT_LIST}


typedef struct player_
{
    char name[30];
    double age;    // years
    unsigned int level;
    char race[30];
    char class[30];
    unsigned int intelligence;
    unsigned int wisdom;
    unsigned int charisma;
    unsigned int strength;
    unsigned int constitution;
    unsigned int dexterity;
    unsigned int resolve; // can fluxuate with fatigue, hunger, fear, hope, proximity of allies, etc.
    unsigned int burden; // the total of the weight of the items carried and equipped
    unsigned int overburdened;
    list_t inventory; // items carried by the player
    //equipped; // items currently equipped
} player;



// function declarations ////////////////////////////////
void initialize_player(player *aPlayer);

#endif // PLAYER_H_
