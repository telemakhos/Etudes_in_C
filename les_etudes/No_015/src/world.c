#include <stdio.h>
#include "world.h"

// function definitions ////////////////////////////////

void initialize_cell_values(world *theWorld) {
    unsigned int count = 0;
    for (unsigned int i=0; i<ROWS; i++) {
        for (unsigned int j=0; j<COLS; j++) {
            theWorld->worldmap[count].row = i;
            theWorld->worldmap[count].col = j;
            theWorld->worldmap[count].index = count;
            theWorld->worldmap[count].temperature = DEFAULT_TEMPERATURE;
            count += 1;
        }
    }
}


void printWorldMap(world *theWorld) {
    printf("%s\n", theWorld->name);
    // print the world map values
    for (unsigned int i=0; i<ROWS*COLS; i++) {
        printf(
            "(%lu %lu):%lu ",
            theWorld->worldmap[i].row,
            theWorld->worldmap[i].col,
            theWorld->worldmap[i].index
        );
        if(theWorld->worldmap[i].col == COLS-1) {
            printf("\n");
        }
    }
    printf("\n"); 
}


void printWorldTemperatures(world *theWorld) {
    printf("Temperatures\n");
    // print the world map values
    for (unsigned int i=0; i<ROWS*COLS; i++) {
        printf("%f ", theWorld->worldmap[i].temperature);
        if(theWorld->worldmap[i].col == COLS-1) {
            printf("\n");
        }
    }
    printf("\n"); 
}
