#include <stdio.h>
#include <string.h>
#include "player.h"
#include "list.h"


// function definitions ////////////////////////////////

void initialize_player(player *thePlayer) {
    strcpy(thePlayer->name, "Starchild");
    thePlayer->level = 0;
    strcpy(thePlayer->race, "human");
    strcpy(thePlayer->class, "fighter");
    thePlayer->intelligence = 10; 
    thePlayer->wisdom = 10;
    thePlayer->charisma = 10;
    thePlayer->constitution = 10;
    thePlayer->dexterity = 10;
    thePlayer->resolve = 50;
    thePlayer->burden = 0;
    thePlayer->overburdened = 0;
    list_t l;
    list_new(&l, sizeof(char *), NULL);
    thePlayer->inventory = l;
}
