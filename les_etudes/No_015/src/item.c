#include <string.h>
#include "item.h"

/* instantiate "crusty bread" */
item init_crusty_bread() {
    item theItem;
    strcpy(theItem.name, "crusty bread");
    theItem.weight = 1;
    theItem.volume = 1;
    theItem.worth = 1;
    strcpy(theItem.modifies, "");
    theItem.modifier = 0;
    theItem.hidden_factor = 0;
    return theItem;
}
