# XOR decryption  
https://projecteuler.net/problem=59  


To compile with GCC:  
`~$ gcc p059.c -o p059 -g -Wall -O3 -std=gnu11 -lm`  
Then run the executable file with:  
`~$ ./p059`  


#### The Problem  
We are given the cipher to decrypt. The cipher, p059_cipher.txt, is simply a long line of integers separated by commas. We are told that the encryption/decryption key consists of three lowercase letters. Recall that ASCII letters are represented by integers. The lowercase English alphabet is represented by the numbers 97 through 122. If we XOR every three-integer chunk of this cipher by the right key, the message will be revealed. To fully solve the problem, according to the webpage, one must sum the integers comprising the plaintext. I don't include that detail here but it is trivial to compute if you can read the plaintext.   

I provide some inline comments and links (below in Sources) to examples I used in creating my solution.  

The problem was a joy to solve. I was, however, disappointed with the plain text I read after the correct key was found. Therefore, I have encrypted my own text (telemaque_cipher.txt) for you to decrypt. This particular line is from a book that evokes much happier memories for me. The key is 5 letters in length and will be familiar to some old-school gamer geeks. Warning it would take quite a while to compute the 5-letter key from all 26 letters, so here's a hint: restrict yourself to the last three (lowercase) letters of the alphabet. What modifications are necessary to make the program work with a 5-letter key?  



#### Sources
dynamically adding to an array implementation modified from:
https://stackoverflow.com/questions/3536153/c-dynamically-growing-array/

intXOR implementation found here:
https://www.geeksforgeeks.org/find-xor-of-two-number-without-using-xor-operator/

how to compute all possible combinations (guesses for the unknown key):
https://stackoverflow.com/questions/40920402/generate-all-possible-combinations-of-array-values-in-c
