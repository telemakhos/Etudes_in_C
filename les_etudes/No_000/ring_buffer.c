/*
...
author: D. R. Forester
*/

# include <stdio.h>
# include <pthread.h>
# include <semaphore.h>
# include <stdlib.h>
# include <string.h>

# define N_FRAMES (8)
# define ROWS (5)
# define COLS (5)
int in = 0;  // new data values go here
int out = 0; // pull out old data values here
static pthread_once_t foo_once = PTHREAD_ONCE_INIT;
static pthread_mutex_t lock;
sem_t countsem, spacesem;


/* a struct to hold our camera frame */
typedef struct _frame {
  char* frameHeader;
  void *imgData;
} Frame;


/* a struct used by pthreads to call enqueue */
struct arg_struct {
    Frame** arg1;
    Frame* arg2;
};


void foo_init() {
    pthread_mutex_init(&lock, NULL);
}


/* initializeBuffer() - allocates a Frame struct to the heap & returns pointer */
void *initializeBuffer(int n_cells) {
  sem_init(&countsem, 0, 0); // number of items in buffer
  sem_init(&spacesem, 0, N_FRAMES); // number of free spaces in buffer
  pthread_once(&foo_once, foo_init);
  Frame *arr = (Frame*)malloc(n_cells * sizeof(Frame));
  if (arr==NULL) {
    return NULL;
  }

  char* initString = ".................";

  for(int i=0; i<n_cells; i++) {
    Frame initFrame;
    initFrame.frameHeader = initString;
    initFrame.imgData = (double*)malloc(ROWS*COLS*sizeof(double));
    arr[i] = initFrame;
  }
  return arr;
}


void* enqueue(void* arguments)
{
  int cvalue;
  sem_getvalue(&countsem, &cvalue);
  int svalue;
  sem_getvalue(&spacesem, &svalue);
  if (cvalue < N_FRAMES) {
    sem_wait( &spacesem );
    pthread_mutex_lock( &lock );
    struct arg_struct *args = arguments;
    memcpy((*(args -> arg1)+in), (args -> arg2), sizeof(Frame));
    in++;
    in = in%(N_FRAMES);
    pthread_mutex_unlock( &lock );
    sem_post( &countsem );
  }
  else {
    printf("*** NO SPACE IN BUFFER; MUST FIRST DEQUEUE ***\n");
    pthread_mutex_unlock( &lock );
  }
}


void* dequeue(void* arguments)
{
  int cvalue;
  sem_getvalue(&countsem, &cvalue);
  int svalue;
  sem_getvalue(&spacesem, &svalue);
  if (cvalue > 0) {
    sem_wait( &countsem );
    pthread_mutex_lock(&lock);
    struct arg_struct *args = arguments;
    memcpy((*(args -> arg1)+out), (args -> arg2), sizeof(Frame));
    out++;
    out = out%(N_FRAMES);
    pthread_mutex_unlock( &lock );
    sem_post( &spacesem );
  }
  else {
    printf("*** BUFFER ALREADY EMPTY; MUST FIRST ENQUEUE ***\n");
    pthread_mutex_unlock( &lock );
  }
}


/* main() - initialize the dynamic memory buffer & write to it. Print buffer
 * to show that main() can access the heap with the buffer address. */
int main()
{
  Frame *buffer = initializeBuffer(N_FRAMES);
  if(buffer == NULL) {
    return -1;
  }

  // Create a blank frames for replacing frames after dequeueing
  char* blankString = ".................";
  double blankImg[ROWS*COLS];
  for(int i=0; i<ROWS*COLS; i++) {
    blankImg[i] = 0.0;
  }
  Frame blankFrame;
  blankFrame.frameHeader = blankString;
  blankFrame.imgData = blankImg;

  Frame* blankFramePtr = &blankFrame;

  // Create a frame; a substitute for an image to store in the buffer
  char* currentString = "data frame header";
  double currentImg[ROWS*COLS];
  for(int i=0; i<ROWS*COLS; i++) {
    currentImg[i] = 5.555555;
  }
  Frame currentFrame;
  currentFrame.frameHeader = currentString;
  currentFrame.imgData = currentImg;

  Frame* framePtr = &currentFrame;

  int running = 1;

  while(running == 1){
      printf( "Select ::: (e)nqueue, (d)equeue, (q)uit: " );
      char option = getchar();
      getchar(); // extra getchar to remove trailing '\n' from user return
      switch( option ){
          case 'q': running = 0; break;
          case 'e': {
             pthread_t e_thread;
             struct arg_struct args;
             args.arg1 = &buffer;
             args.arg2 = framePtr;
             pthread_create(&e_thread, NULL, &enqueue, (void *)&args);
             pthread_join(e_thread, NULL);
             break;
          }
          case 'd': {
             pthread_t d_thread;
             struct arg_struct args;
             args.arg1 = &buffer;
             args.arg2 = blankFramePtr;
             pthread_create(&d_thread, NULL, &dequeue, (void *)&args);
             pthread_join(d_thread, NULL);
             break;
          }
          case EOF: running = 0; break;
      }
      printf("value of the counter  \"in\": %d\n", in);
      printf("value of the counter \"out\": %d\n", out);
      int value;
      sem_getvalue(&countsem, &value);
      printf("value of semaphore \"countsem\": %d\n", value);
      sem_getvalue(&spacesem, &value);
      printf("value of semaphore \"spacesem\": %d\n", value);
      printf("\n");
      printf("--- printing header and 1st...last values in buffer ---\n");
      for(int i=0; i<N_FRAMES; i++) {
        char* thisFrameHeader = buffer[i].frameHeader;
        double* thisImgData = buffer[i].imgData;
        printf("%s  %f...%f\n", thisFrameHeader, thisImgData[0], thisImgData[(ROWS*COLS)-1]);
      }
      printf("\n\n");
  }

  free(buffer);
  return 0;
}
