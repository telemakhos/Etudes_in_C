# include <stdio.h>
# include <stdlib.h>


/*
initializeBuffer()
allocates an array of floats to the heap & returns pointer
 */
void *initializeBuffer(int n_cells) {
  float *arr = (float*)malloc(n_cells * sizeof(float));
  if (arr==NULL) {
    return NULL;
  }
  for(int i=0; i<n_cells; i++) {
    arr[i] = 0.0;
  }
  return arr;
}


/*
enqueue()
writes a float value to a given index of the buffer
*/
void enqueue(float **arr, float value, int idx)
{
  *(*arr+idx) = value;
}


/*
initialize the dynamic memory buffer & write to it. Print buffer
to show that main() can access the heap with the buffer address.
*/
int main()
{
  int num_frames = 8;
  float *buffer = initializeBuffer(num_frames);
  if(buffer == NULL) {
    return -1;
  }

  printf("initialized buffer:\n");
  for(int i=0; i<num_frames; i++)
  {
    printf("%f\n", buffer[i]);
  }

  // insert a value at an index of buffer
  enqueue(&buffer, 3.33, 0);
  enqueue(&buffer, 5.55, 2);
  enqueue(&buffer, 7.77, 4);

  printf("\nmodified buffer:\n");
  for(int i=0; i<num_frames; i++)
  {
    printf("%f\n", buffer[i]);
  }

  free(buffer);
  return 0;
}
