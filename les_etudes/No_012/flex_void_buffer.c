#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#define OFFSETOF(TYPE, ELEMENT) ((size_t)&(((TYPE *)0)->ELEMENT))

/* A buffer structure containing 'count' entries of the given size */
typedef struct {
    size_t size;
    int count;
    void *buf;
} buffer_t;

/* Allocate a new buffer_t with 'count' entries of 'size' size */
buffer_t *buffer_new(size_t size, int count)
{
    buffer_t *p = malloc(OFFSETOF(buffer_t, buf) + count*size);
    if (p) {
        p->size = size;
        p->count = count;
    }
    return p;
}

/* Get a pointer to the i'th entry */
void *buffer_get(buffer_t *t, int i)
{
    return &t->buf + i * t->size;
}

int main()
{
    int sz = 1; // sizeof(size_t) = 8 bytes on for 64-bit compiler (typedef for unsigned long long)
    int cnt = 5;   // number of elements
    buffer_t * btp1 = buffer_new(sz, cnt);

    unsigned long long *point = (unsigned long long *) buffer_get(btp1, 0);
    unsigned long long val = 666;
    printf("This is a dynamic array of unsigned long long values.\n");
    for (int i=0; i<cnt; i++) {
        *point = val;
        printf( "value at index %d -> %llu   address is: %p\n", i, (unsigned long long)point[0], point );
        point += 1;
        val += 1;
    }

    printf("\n");

    sz = 1; // sizeof(size_t) = 8 bytes on for 64-bit compiler (typedef for unsigned long long)
    cnt = 6;   // number of elements
    buffer_t * btp2 = buffer_new(sz, cnt);

    double *point2 = (double *) buffer_get(btp2, 0);
    double val2 = 666.666;
    printf("This is a dynamic array of double float values.\n");
    for (int i=0; i<cnt; i++) {
        *point2 = val2;
        printf( "value at index %d -> %f   address is: %p\n", i, (double)point2[0], point2 );
        point2 += 1;
        val2 += 1;
    }
}
