/* 
   Find the greatest prime factor, if any, of a given integer. Here 2 is not
   considered a valid common factor.
*/

// To compile with gcc:
// gcc p003.c -o p003 -g -Wall -O3 -std=gnu11 -lm

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>


int is_prime(long int x);


int main()
{
    //int N = 13195;
    //int N = 13198;
    //int N = 1000;
    int N = 51;
    //long int N = 600851475143;

    long int half = N/2;
    if( half % 2 == 0 ) half += 1;
    long int ii = half;


    while( (is_prime(ii)==0) || (N % ii != 0) ) {
        
        printf("NO %d:", ii);
        ii -= 2;
        printf("%d ", ii);
        if(ii < 2) break;
    }
    printf("%d\n", ii);
  
    if(ii > 1){
        printf("The greatest prime factor of %ld is %ld.\n", N, ii);
    }
    else{
        printf("No gpf found.\n");
    }
}


int is_prime(long int N) {

    if ( (N==2)||(N==3)||(N==5)||(N==7) ) {
        return 1;
    }

    if ((N % 2 == 0) || (N < 2)) {
        return 0;
    }

    long int sqroot = sqrt(N);
    if( sqroot%2 == 0 ){
        sqroot = sqroot-1;
    }

    int isprime = 0;
    long int j = sqroot;

    while(N % j != 0) {
        j -= 2;
        if(j == 1) isprime = 1;
    }
    return isprime;
}
