## Calculating the greatest prime factor of an integer.
This program relies on the function is_prime(), included in the single file prog.c. For a given integer x, a variable ii is set to x//2. A primality check and a divisibility of x by ii check are made. If either of these checks fails, then ii is decremented by 2 (so that it is always equal to an odd int). If the two checks are positive during any iteration, then the greatest prime factor has been found, otherwise, none exists. I do not here include 2 as a prime factor.  

### Instructions  

Choose an integer to check, such as N = 86  
Enter this in the top of main() in the file prob.c  
Then,  
```
~$ gcc prob.c -o prob -g -Wall -O3 -std=gnu11 -lm
~$ ./prob
```

#### Sources  

1. To test for primality of x, we need only check for divisibility by ints <= sqrt(x):
https://math.stackexchange.com/questions/46342/is-there-a-non-prime-number-that-is-divisible-only-by-numbers-greater-than-its-s  


#### Suggested Improvements  

1. Are there any more known properties of primes that we can use to speed up the calculation?  
