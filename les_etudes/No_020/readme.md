### Google Billboard Challenge  

from the [Wikipedia article](https://en.wikipedia.org/wiki/E_\(mathematical_constant\)#In_computer_culture) on Euler's number:  

Google was also responsible for a billboard that appeared in the heart of Silicon Valley, and later in Cambridge, Massachusetts; Seattle, Washington; and Austin, Texas. It read "{first 10-digit prime found in consecutive digits of e}.com". The first 10-digit prime in e is 7427466391, which starts at the 99th digit.[46] Solving this problem and visiting the advertised (now defunct) website led to an even more difficult problem to solve, which consisted in finding the fifth term in the sequence 7182818284, 8182845904, 8747135266, 7427466391. It turned out that the sequence consisted of 10-digit numbers found in consecutive digits of e whose digits summed to 49. The fifth term in the sequence is 5966290435, which starts at the 127th digit. Solving this second problem finally led to a Google Labs webpage where the visitor was invited to submit a résumé.  


#### This [repository](./No_19)  

The program [billboard.c](./billboard.c) reads in the first 10,000 digits of **e** from a text file and performs a primality check on each sub-string of length 10 after converting from a string to a long integer. This provides the solution to the first part of the billboard problem above.  

To solve the second part, the included program can be easily modified to check which 10-digit numbers sum to 49.  

