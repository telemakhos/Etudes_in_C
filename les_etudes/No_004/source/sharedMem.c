// Create the .o file
// 1. ~$ gcc -Wall -fPIC -c sharedMem.c
//
// Build the shared library
// 2. ~$ gcc -shared -Wl,-soname,libsharedMem.so.1 -o libsharedMem.so.1.0 sharedMem.o
//
// 3. ~$ ln -sf libsharedMem.so.1.0 libsharedMem.so.1
//
// 4. ~$ ln -sf libsharedMem.so.1 libsharedMem.so

// 5. Add to ld.so.conf.d
// ~$ cd /etc/ld.so.conf.d
// ~$ sudo touch sharedMem.conf
// ~$ sudo nano sharedMem.conf
// add the following line:
//     /home/telemaque/1210SRD/PRACTICE/testing/prod_cons/XX/source

/*
Shared Memory Implementation
*/

#include "../include/sharedMem.h"


// Create a zero-length POSIX shared memory object
// * the "name" string should begin with "/"
int shm_create(const char *name) {

    int flags, fd;
    mode_t perms;
    size_t size;
    void *addr;

    flags = O_RDWR | O_CREAT ;
    perms = 0666;
    size = 1;

    // Create shared memory object ans set its size
    fd = shm_open(name, flags, perms);
    if (fd == -1) {
        printf("create: Shared memory failed: %s\n", strerror(errno));
        exit(1);
    }

    if (ftruncate(fd, size) == -1) {
        printf("write: ftruncate error: %s\n", strerror(errno));
    }

    // Map shared memory object
    addr = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (addr == MAP_FAILED) {
        printf("create: Map failed: %s\n", strerror(errno));
        exit(1);
    }

    exit(EXIT_SUCCESS);

}


/* Write to the existing POSIX shared memory object */
void shm_write(const char *name, void *arr, int nbytes) {

    int flags, fd;
    void *addr;

    flags = O_RDWR;

    // open existing object
    fd = shm_open(name, flags, 0);
    if (fd == -1) {
      printf("write: Shared memory failed: %s\n", strerror(errno));
      exit(1);
    }

    // resize object to hold array
    if (ftruncate(fd, nbytes) == -1) {
        printf("write: ftruncate error: %s\n", strerror(errno));
    }
    printf("Resized to %ld bytes\n", (long) nbytes);

    addr = mmap(NULL, nbytes, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (addr == MAP_FAILED) {
        printf("write: Map failed: %s\n", strerror(errno));
        exit(1);
    }

    // 'fd' is no longer needed
    if (close(fd) == -1) {
        printf("write: Close failed: %s\n", strerror(errno));
        exit(1);
    }

    // copy array to shared memory
    printf("copying %d bytes\n", nbytes);
    memcpy(addr, arr, nbytes);

}


/* Read from an existing POSIX shared memory object */
void * shm_read(const char *name) {

    int flags, fd;
    int *addr;
    struct stat sb;

    flags = O_RDONLY;

    // open existing object
    fd = shm_open(name, flags, 0);
    if (fd == -1) {
      printf("read: Shared memory failed: %s\n", strerror(errno));
      exit(1);
    }

    // use shared memory object size as length argument for mmap()
    if (fstat(fd, &sb) == -1) {
        printf("read: fstat failed: %s\n", strerror(errno));
        exit(1);
    }


    addr = mmap(NULL, sb.st_size, PROT_READ, MAP_SHARED, fd, 0);
    if (addr == MAP_FAILED) {
        printf("read: Map failed: %s\n", strerror(errno));
        exit(1);
    }

    // 'fd' is no longer needed
    if (close(fd) == -1) {
        printf("read: Close failed: %s\n", strerror(errno));
        exit(1);
    }

    return addr;

}


/* remove the existing shared memory segment, identified by "name", from the file system */
int shm_remove(const char *name) {

    if (shm_unlink(name) == -1) {
      printf("cons: Error removing %s: %s\n", name, strerror(errno));
      exit(1);
    }

    exit(EXIT_SUCCESS);

}
