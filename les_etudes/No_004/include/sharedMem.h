#ifndef SHAREDMEM_H
#define SHAREDMEM_H


#include <stdio.h>
#include <stdlib.h>
#include <sys/shm.h>            // for shm_open() and shm_unlink()
#include <string.h>             // for memcpy
#include <sys/stat.h>           // defines mode constants for shm_open()
#include <fcntl.h>              // defines O_* constants for shm_open()
#include <sys/mman.h>           // for mmap
#include <unistd.h>             // for ftruncate
#include <errno.h>


/* Create a zero-length POSIX shared memory object */
int shm_create(const char *name);

/* Write to the existing POSIX shared memory object */
void shm_write(const char *name, void *arr, int nbytes);

/* Read from an existing POSIX shared memory object */
// returns a pointer to the array in virtual memory
void * shm_read(const char *name);

/* remove the existing shared memory segment from the file system */
int shm_remove(const char *name);

#endif
