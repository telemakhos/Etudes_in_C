// ~$ gcc -Wall -I./include -L./source unlink_segment.c -lsharedMem -o unlink -lrt

/* Testing sharedMem.c */

#include "./include/sharedMem.h"

int main() {

    const char *name = "/testing";
    shm_remove(name);
}
