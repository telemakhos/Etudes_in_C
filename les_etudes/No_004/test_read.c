// ~$ gcc -Wall -I./include -L./source test_read.c -lsharedMem -o reading -lrt

/* Testing sharedMem.c */

#include <time.h>
#include "./include/sharedMem.h"

#define ROWS (3)
#define COLS (5)

int main() {

    // the name of the existing shared memory segment
    const char *name = "/testing";

    useconds_t usec = 150000;

    //int *p;
    long int *p;
    int nreads = 10; // read the shm this many times
    for (int i=0; i<nreads; i++) {

        // get a pointer to the array in virtual memory
        p = shm_read(name);

        // write the array just read
        int count = 0;
        for (int j=0; j<ROWS; j++) {
            for (int k=0; k<COLS; k++) {
                //printf( "%d ", *(p+count) );
                printf( "%ld ", *(p+count) );
                count++;
            }
            printf("\n");
        }
        printf("\n");

        // wait until next array is written (simulated...)
        usleep(usec);
    }
}
