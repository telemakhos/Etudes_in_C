# A Shared Memory Implementation  

A ring buffer similar to the one we looked at in No_000 of this repository will need to live in memory somewhere on my system. Since I envision having some process create then periodically writing to this memory segment, and another unrelated process periodically reading from it, I will have the Linux kernel manage a shared memory object with read + write access. This will be easiest to use by both processes if we implement it as a shared library.


# Instructions  

Do the following:  

```
~$ cd source  
~$ gcc -Wall -fPIC -c sharedMem.c  
~$ gcc -shared -Wl,-soname,libsharedMem.so.1 -o libsharedMem.so.1.0 sharedMem.o  
~$ ln -sf libsharedMem.so.1.0 libsharedMem.so.1  
~$ ln -sf libsharedMem.so.1 libsharedMem.so  
```
Now that the shared library, the ".so" file and it's symlinks, are in sources, cd back up a level and compile the example programs that will use it.  
```
~$ cd ..  
~$ gcc -Wall -I./include -L./source create_segment.c -lsharedMem -o create -lrt  
~$ gcc -Wall -I./include -L./source test_write.c -lsharedMem -o writing -lrt  
~$ gcc -Wall -I./include -L./source test_read.c -lsharedMem -o reading -lrt  
~$ gcc -Wall -I./include -L./source unlink_segment.c -lsharedMem -o unlink -lrt
```
Test the example programs. First run ./create; it will create a 1-byte shared memory object that we can write to and read from. Verify that a shared memory object named "testing" is there:  
```
~$ ls -l /dev/shm
```
Then run ./writing followed by ./reading. Notice that the ./reading example reads 10 times once every 150 milliseconds. Finally, unlink the object with ./unlink .  


#### Sources  

1. POSIX Shared Memory Example:
http://www.cse.psu.edu/~deh25/cmpsc473/notes/OSC/Processes/shm.html

2. ***The Linux Programming Interface***, by Michael Kerrisk; No Starch Press; 1 edition (October 28, 2010),
Chapter 54, "POSIX Shared Memory".


#### Suggested Improvements  

1. Use makefiles, sheesh!  
