/*
Sorts ascending, then searches in array for an instance of a particular value
*/

// NOTE: The binary search functions first() and last() were copied from:
// https://www.geeksforgeeks.org/find-first-and-last-positions-of-an-element-in-a-sorted-array/
//
//COMPILE: gcc search_first_last.c -o search -g -Wall -O3 -std=gnu11

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

int cmpfunc(const void * a, const void * b);
int first(int arr[], int low, int high, int x, int n);
int last(int arr[], int low, int high, int x, int n);


int main()
{
    /* Intialize random number generator */
    time_t t;
    srand((unsigned) time(&t));

    /* Create an array of N random values from the interval [a, b) */
    int N = 20;   // number of array elements
    int arr[N];   // an array to sort and search
    int a = 140;  // lower bound for values
    int b = 160;  // upper bound for values
    for (int ii=0; ii<N; ii++) {
        arr[ii] = rand()%(b-a) + a;
        printf("%d: %d\n", ii, arr[ii]);
    }

    /* Sort ascending */
    qsort(arr, N, sizeof(int), cmpfunc);
    printf("sorting...\n");
    for (int ii=0; ii<N; ii++) {
        printf("%d: %d\n", ii, arr[ii]);
    }

    /* Binary Search */
    int key = 150; // the value sought in the array
    int idx_first = first(arr, 0, N-1, key, N);
    int idx_last = last(arr, 0, N-1, key, N);
    if (idx_first != -1) {
        printf("First occurance of %d found at index %d\n", key, idx_first);
        printf("Last occurance of %d found at index %d\n", key, idx_last);
    }
    else
        printf("The key value %d could not be found\n", key);
}


/* Compare function for qsort() and bsearch() */
int cmpfunc(const void * a, const void * b) {
   return ( *(int*)a - *(int*)b );
}


/* if x is present in arr[] then returns the index of
   FIRST occurrence of x in arr[0..n-1], otherwise
   returns -1 */
int first(int arr[], int low, int high, int x, int n)
{
    if(high >= low)
    {
        int mid = low + (high - low)/2;
        if( ( mid == 0 || x > arr[mid-1]) && arr[mid] == x)
            return mid;
        else if(x > arr[mid])
            return first(arr, (mid + 1), high, x, n);
        else
            return first(arr, low, (mid -1), x, n);
    }
    return -1;
}


/* if x is present in arr[] then returns the index of
   LAST occurrence of x in arr[0..n-1], otherwise
   returns -1 */
int last(int arr[], int low, int high, int x, int n)
{
    if (high >= low)
    {
        int mid = low + (high - low)/2;
        if (( mid == n-1 || x < arr[mid+1]) && arr[mid] == x)
            return mid;
        else if (x < arr[mid])
            return last(arr, low, (mid -1), x, n);
        else
            return last(arr, (mid + 1), high, x, n);
    }
    return -1;
}
