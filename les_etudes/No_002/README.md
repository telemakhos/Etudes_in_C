# Sort then search an array for a value    


To compile with GCC:  
`~$ gcc search.c -o search -g -Wall -O3 -std=gnu11`   
Then run the executable file with:  
`~$ ./search`  


#### Program Outline  
1. Create an array of N random integer values in some range a<=i<b.
2. Sort the array using the standard library qsort().
3. Search the array using the standard library bsearch().  


#### Improvements  
The standard library function bsearch(), which was employed in the first program [search.c](./search.c), returns a pointer to a matching member of the array, or NULL if no match is found. If the array has multiple matching elements the return value will be a pointer to one of those elements. Which particular element is unspecified. Usually one wants to find the index of the first and/or the last matching element. To provide this functionality, we must provide our own binary search functions, one which returns the index of the first instance, and one which returns the index of the last.  

An example of how to implement this kind of search on sorted arrays can be found here: https://www.geeksforgeeks.org/find-first-and-last-positions-of-an-element-in-a-sorted-array/. In fact, I simply copied the first() and last() functions from the article and used them in a new program named [search_first_last.c](./search_first_last.c).
