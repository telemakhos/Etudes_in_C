## Summing numbers too large to be represented by any data type.
How would you sum a collection of numbers, each too large to be represented by a data type availible to your processor/compiler? The answer is to break apart each number into smaller numbers and sum these separately, keeping track of the carries.

In this example "largesums.c", 100 50-digit numbers are first represented as strings so that they can be easily broken into 5 equal columns of "unsigned long long" integers. The least significant column is summed first and its carry saved. Then the next more significant column is summed and added to the first carry, then its carry is saved. This continues until the fifth and final column is summed and added to the previous carry.

### Instructions  

Compile then run:  
```
~$ gcc largesums.c -o largesums -g -Wall -O3 -std=gnu11
~$ ./largesums
```
