#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "vector.h"


// allocate more memory to the elements array
static void vector_grow(vector_t *vector) {
    if (vector->allocatedLength == 0) {
        vector->allocatedLength += 2;
        //printf("    setting allocatedLength to 2\n");
    }
    else {
        vector->allocatedLength *= 2;
        //printf("    setting allocatedLength *=2\n");
    }
    vector->elements =
        realloc(vector->elements, vector->elementSize * vector->allocatedLength);
    assert(vector->elements);
}


// get the pointer to an element
static void *vector_address(vector_t *vector, int index) {
    int addr = vector->elementSize * index;
    return (char *)vector->elements + addr;
}


// copy a value from one index to another; used by vector_remove_at()
static void vector_copy_item(vector_t *vector, int sourceIndex, int destIndex) {
    void *source = vector_address(vector, sourceIndex);
    void *target = vector_address(vector, destIndex);
    memcpy(target, source, vector->elementSize);
}


// create a vector_t type struct
void vector_new(vector_t *vector, int elementSize, freeFunction freeFn) {
    assert(elementSize > 0);    
    vector->elementSize = elementSize;
    vector->logicalLength = 0;
    vector->allocatedLength = 0;
    vector->elements = NULL;
    vector->freeFn = freeFn;
}


// free elements; using freeFn if one provided
void vector_destroy(vector_t *vector) {
    if (vector->freeFn) {
        int i;
        for (i = 0; i < vector_size(vector); i++) {
            vector->freeFn(vector_address(vector, i));
        }
    }
    free(vector->elements);
}


// get vector "used" length
int vector_size(vector_t *vector) {
    return vector->logicalLength;
}


// add an element to the end of the "used" portion
void vector_add(vector_t *vector, void *element) {
    if (vector->allocatedLength == vector->logicalLength) {
        vector_grow(vector);
    }
    void *target = vector_address(vector, vector->logicalLength++);
    memcpy(target, element, vector->elementSize);
}


// get element value at index
void vector_item_at(vector_t *vector, int index, void *target) {
    assert(index >= 0 && index < vector->logicalLength);    
    void *source = vector_address(vector, index);
    memcpy(target, source, vector->elementSize);
}


// insert element at index
void vector_insert_at(vector_t *vector, int index, void *target) {
    assert(index >= 0 && index <= vector->logicalLength);
    vector_add(vector, target);
    if (index < vector->logicalLength) {
        int i;
        void *source;
        void *destination;
        //for (i = vector->logicalLength - 2; i > index; i--) {
        for (i = vector->logicalLength-1; i > index; i--) {
            source = vector_address(vector, i);
            destination = vector_address(vector, i + 1);
            memcpy(destination, source, vector->elementSize);
        }
        destination = vector_address(vector, i);
        memcpy(destination, target, vector->elementSize);
    }
}


// remoe an element at index
void vector_remove_at(vector_t *vector, int index) {
    assert(index >= 0 && index < vector->logicalLength);
    // remove the item
    void *item = vector_address(vector, index);
    if (vector->freeFn) {
        vector->freeFn(item);
    }
    while (++index < vector->logicalLength) {
        vector_copy_item(vector, index, index - 1);
    }
    vector->logicalLength--;
}
