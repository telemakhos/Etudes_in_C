#include <stdio.h>
#include "vector.h"

int main(void)
{
    int initLength = 10;
  
    vector_t v;
    vector_new(&v, sizeof(char), NULL);
    printf("allocatedLength: %d\n", v.allocatedLength);
  
    for (int i = 0; i < initLength; i++) {
        vector_add(&v, &v);
        printf("allocatedLength: %d\n", v.allocatedLength);
    }
  
    char value = 'A';
    // prepend value
    vector_insert_at(&v, 0, &value);
  
    // insert in the middle
    value = 'H';
    vector_insert_at(&v, 5, &value);
  
    // add to end
    value = 'Z';
    vector_insert_at(&v, v.logicalLength, &value);
  
    vector_item_at(&v, 0, &value);
    printf("value at 0: %c\n", value);
  
    vector_item_at(&v, 5, &value);
    printf("value at 5: %c\n", value);
  
    vector_item_at(&v, v.logicalLength - 1, &value);
    printf("value at len-1: %c\n", value);

    vector_destroy(&v);

}
