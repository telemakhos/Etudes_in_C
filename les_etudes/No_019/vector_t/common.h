#ifndef COMMON_H
#define COMMON_H

// a common function used to free malloc'd objects
typedef void (*freeFunction)(void *);

#endif
