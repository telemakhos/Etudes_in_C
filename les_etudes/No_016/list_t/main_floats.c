#include <stdlib.h>
#include <stdio.h>
#include "list.h"

void print_list_double(list_t *list);

int main(void)
{
    unsigned long int i; // loop index
    double val;          // list element

    // test list_new()
    list_t l;
    list_new(&l, sizeof(double), NULL);
    print_list_double(&l);

    // test list_append()
    printf("list_append() loop\n");
    for (i = 0; i < 10; i++) {
        val = i * 10.000;
        list_append(&l, &val);
    }
    print_list_double(&l);

    // test list_prepend()
    val = 666.666;
    list_prepend(&l, &val);
    printf("list_prepend()\n");
    print_list_double(&l);

    // test insert_at() (HEAD)
    val = 999.999;
    list_insert_at(&l, 0, &val);
    printf("list_insert_at(0)\n");
    print_list_double(&l);

    // test insert_at() (TAIL)
    val = 333.333;
    list_insert_at(&l, 12, &val);   
    printf("list_insert_at(12)\n");
    print_list_double(&l);
 
    // test_list_insert_at()
    val = 0.987;
    list_insert_at(&l, 5, &val);
    printf("list_insert_at(5)\n");
    print_list_double(&l);

    //// test_list_set_at()
    //val = 987654321.0;
    //list_set_at(&l, 6, &val);
    //printf("list_set_at(5)\n");
    //print_list_double(&l);

    //// test list_rm_head()
    //list_rm_head(&l);
    //printf("list_rm_head()\n");
    //print_list_double(&l);

    //// test list_rm_tail()
    //list_rm_tail(&l);
    //printf("list_rm_tail()\n");
    //print_list_double(&l);

    //// test list_rm_tail()
    //list_rm_tail(&l);
    //printf("list_rm_tail()\n");
    //print_list_double(&l);

    //// test list_rm_at()
    //list_rm_at(&l, 10);
    //printf("list_rm_at(10)\n");
    //print_list_double(&l);

    //// test list_rm_at()
    //list_rm_at(&l, 4);
    //printf("list_rm_at(4)\n");
    //print_list_double(&l);

    //// test list_rm_at()
    //list_rm_at(&l, 4);
    //printf("list_rm_at(4)\n");
    //print_list_double(&l);


    list_free(&l);
    
}


void print_list_double(list_t *list) {
    size_t sz = list_size(list);
    printf("sz: %lu\n", sz);
    double val;
    for(unsigned long int i=0; i<sz; i++) {
        list_get_at(list, i, &val);
        printf("%lu : %f\n", i, val);
    }
    printf("\n");
}
