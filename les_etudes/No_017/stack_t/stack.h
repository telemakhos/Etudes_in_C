#ifndef __STACK_H
#define __STACK_H

#include "common.h"
#include "list.h"


typedef struct {
    list_t *list;
} stack_t;

void stack_new(stack_t *s, int elementSize, freeFunction freeFn);

void stack_free(stack_t *s);

void stack_push(stack_t *s, void *element);

void stack_pop(stack_t *s);

int stack_size(stack_t *s);

#endif
