#include <stdlib.h>
#include <stdio.h>
#include "stack.h"


void print_stack_int(stack_t *s);

int main(void)
{
    stack_t s;
    stack_new(&s, sizeof(int), NULL);

    int value = 10;
    stack_push(&s, &value);
    print_stack_int(&s);


    stack_free(&s);

}


void print_stack_int(stack_t *stack) {
    size_t sz = list_size(stack->list);
    printf("sz: %lu\n", sz);
    int val; // data type of stack elements
    for(unsigned long int i=0; i<sz; i++) {
        list_get_at(stack->list, i, &val);
        printf("%lu : %d\n", i, val);
    }
    printf("\n"); 
}
